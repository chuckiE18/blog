<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Repositories\CategoryRepositoryInterface;


class CategoryController extends Controller
{
    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $categories = $this->categoryRepository->allPaginate(5);

        return view('categories.index', compact('categories'));
    }

    public function show($slug)
    {
        $category = $this->categoryRepository->getBySlug($slug);
        $posts = $this->categoryRepository->getPostsWithPaginate($category, 3);

        return view('categories.show', compact('category', 'posts'));
    }
}
