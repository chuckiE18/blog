<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Models\Request;


class ContactController extends Controller
{
    public function index()
    {
        return view('contact');
    }

    public function store(ContactRequest $request)
    {
        Request::create($request->validated());

        return redirect()->back()->with('success', 'Ваше сообщение отправлено');
    }
}
