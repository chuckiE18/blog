<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        Cache::forget('posts.index');
        $posts = Cache::remember('posts.index', 60*60*24, function () {
            return $this->postRepository->allWithCategoriesOrder();
        });

        return view('index', compact('posts'));
    }

    public function show($slug)
    {
        $post = $this->postRepository->findBySlug($slug);

        return view('posts.show', compact('post'));
    }
}
