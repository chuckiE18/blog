<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreate;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserLoginRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function loginForm()
    {
        return view('login');
    }

    public function create()
    {
        return view('register');
    }

    public function store(UserCreateRequest $request)
    {
        $user = $this->userRepository->register($request->validated());
        if ($user) {
           return redirect()->route('loginForm')->with('register', 'Вы зарегистрированы');
        }
    }

    public function login(UserLoginRequest $request)
    {
        $remember = false;
        $validated  = $request->validated();
        if (!empty($validated['remember'])) {
            $remember = true;
        }

        if (Auth::attempt(['email' => $validated['email'], 'password' => $validated['password']], $remember)) {
            $request->session()->regenerate();
            session()->flash('success', 'Вы успешно авторизованы');
            if (Auth::user()->is_admin) {
                return redirect()->route('admin.index');
            }
            return redirect()->route('home');
        }

        return redirect()->back()->withErrors(['Неправильный логин или пароль']);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->home();
    }
}
