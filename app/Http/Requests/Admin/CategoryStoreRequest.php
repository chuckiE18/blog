<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:30'],
            'slug'  => ['required', 'unique:categories']
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Название должно быть заполнено',
            'title.max' => 'Название не должно превышать 30 символов',
            'slug.required' => 'Slug должен быть заполнен',
            'slug.unique' => 'Категория с таким slug уже существует'
        ];
    }
}
