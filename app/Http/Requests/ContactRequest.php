<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => ['required', 'max:30'],
            'email'   => ['required', 'email'],
            'phone'   => ['required', 'regex:/^(8|\+7)?\d{10}$/'],
            'message' => ['required', 'max:255']
        ];
    }

    public function messages()
    {
        return [
            'name.required'    => 'Поле Имя должно быть заполнено',
            'email.required'   => 'Поле Email должно быть заполнено',
            'phone.required'   => 'Поле Телефон должно быть заполнено',
            'phone.regex'      => 'Проверьте правильность ввода Телефона',
            'message.required' => 'Поле сообщение должно быть заполнено'
        ];
    }
}
