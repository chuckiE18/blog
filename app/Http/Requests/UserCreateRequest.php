<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|max:20',
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:8|max:16|confirmed'
        ];
    }

    public function messages()
    {
        return [
            'name.required'      => 'Поле имя обязательно для заполнения',
            'name.max'           => "Поле имя должно быть короче 15 символов",
            'email.required'     => 'Поле email обязательно для заполнения',
            'email.email'        => 'Указан некорректный email',
            'email.unique'       => 'Пользователь с таким email уже существует',
            'password.required'  => 'Поле пароль обязательно для заполнения',
            'password.min'       => "Поле пароль должно быть более 8 символов",
            'password.max'       => "Поле пароль должно быть короче 16 символов",
            'password.confirmed' => "Введённые пароли не совпадают",
        ];
    }
}
