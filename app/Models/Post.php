<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'description',
        'content',
        'category_id',
        'picture'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

//    public function getCreatedAtAttribute($value)
//    {
//        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d F, Y');
//    }
}
