<?php


namespace App\Repositories;


use App\Models\Category;


class CategoryRepository implements CategoryRepositoryInterface
{
    public function all()
    {
       return Category::all();
    }

    public function allPaginate($count = 10)
    {
        return Category::paginate($count);
    }

    public function store($data)
    {
        return Category::create($data);
    }

    public function getById($id)
    {
        return Category::findOrFail($id);
    }

    public function update($id, $data)
    {
        $category = Category::findOrFail($id);
        $category->update($data);
    }

    public function getBySlug($slug)
    {
        return Category::where('slug', $slug)->firstOrFail();
    }

    public function getPostsWithPaginate($category, $count = 5)
    {
        return $category->posts()->paginate($count);
    }
}
