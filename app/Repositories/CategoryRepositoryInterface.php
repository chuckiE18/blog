<?php


namespace App\Repositories;


use App\Models\Category;

interface CategoryRepositoryInterface
{
    public function all();

    public function allPaginate($count = 10);

    public function store($data);

    public function getById($id);

    public function update($id, $data);

    public function getBySlug($slug);

    public function getPostsWithPaginate($category, $count = 5);
}
