<?php


namespace App\Repositories;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class PostRepository implements PostRepositoryInterface
{
    public function allWithCatagories($count = 10)
    {
        return Post::with('category')->paginate($count);
    }
    public function allWithCategoriesOrder($count = 10, $column = 'id', $order = 'desc')
    {
        return Post::with('category')->orderBy($column, $order)->paginate($count);
    }

    public function store($data)
    {
        $data['picture'] = $data['picture']->store('images');
        Post::create($data);
    }

    public function edit($slug)
    {
        // TODO: Implement edit() method.
    }

    public function destroyById($id)
    {
        $post = Post::findOrFail($id);

        $post->delete();
    }

    public function findById($id)
    {
        return Post::findOrFail($id);
    }

    public function update($id, $data)
    {
       $post = Post::findOrFail($id);

       if (!empty($data['picture'])) {
           Storage::delete($post->picture);
           $data['picture'] = $data['picture']->store('images');
       } else {
           $data['picture'] = $post->picture;
       }

       $post->update($data);
    }

    public function findBySlug($slug)
    {
        return Post::where('slug', $slug)->firstOrFail();
    }
}
