<?php


namespace App\Repositories;



interface PostRepositoryInterface
{
    public function allWithCatagories($count = 10);

    public function allWithCategoriesOrder($count = 10, $column = 'id', $order = 'desc');

    public function store($data);

    public function edit($slug);

    public function destroyById($id);

    public function findById($id);

    public function update($id, $data);

    public function findBySlug($slug);
}
