<?php


namespace App\Repositories;


use App\Models\User;

class UserRepository implements UserRepositoryInterface
{

    public function all($count = 10)
    {
        return User::paginate($count);
    }

    public function findById($userId)
    {
//        return User::where('id', $userId);
        return User::findOrFail($userId);
    }

    public function store($validated)
    {
        $validated['is_admin'] = isset($validated['is_admin']) ? 1 : 0;

        return User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => bcrypt($validated['password']),
            'is_admin' => $validated['is_admin']
        ]);
    }

    public function update($userId, $validated)
    {
        $user = User::where('id', $userId)->firstOrFail();
        $user->name = $validated['name'];
        $user->email = $validated['email'];
        if (isset($validated['password']) && !empty($validated['password'])) {
            $user->password = bcrypt($validated['password']);
        }
        $user->is_admin = isset($validated['is_admin']) ? 1 : 0;
        $user->save();
//        $user->update(request()->only('name'));
    }

    public function destroyById($userId)
    {
        $user = User::findOrFail($userId);
        $user->delete();
    }

    public function register($validated)
    {
        return User::create([
            'name'     => $validated['name'],
            'email'    => $validated['email'],
            'password' => bcrypt($validated['password'])
        ]);
    }

    public function format($user)
    {
        return [
            'user_id' => $user->id,
            'name' => $user->name,
            'email' => $user->email
        ];
    }
}
