<?php


namespace App\Repositories;



interface UserRepositoryInterface
{
    public function all($count);

    public function findById($userId);

    public function store($data);

    public function update($userId, $data);

    public function destroyById($userId);

    public function register($data);

    public function format($user);
}
