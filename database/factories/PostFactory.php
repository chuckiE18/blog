<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->words('4', true),
            'slug'  => $this->faker->word(),
            'description'  => $this->faker->paragraph(1),
            'content'  => $this->faker->paragraph(5),
            'picture'  => 'assets/img/post-bg.jpg',
            'category_id'  => $this->faker->numberBetween(1,5),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime()
        ];
    }
}
