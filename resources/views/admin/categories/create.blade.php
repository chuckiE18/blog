@extends('admin.layouts.layout')

@section('content')
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Создание категории</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-toolbar mb-2 mb-md-0">
                    <a href="{{ route('admin.categories.index') }}" class="btn btn-primary">Назад</a>
                </div>
            </div>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-lg-4">
            <form method="POST" action="{{ route('admin.categories.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputTitle" class="form-label">Название</label>
                    <input name="title" type="text" class="form-control" id="exampleInputTitle" value="{{ old('title') }}" required>
                    <div id="nameHelp" class="form-text"></div>
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="exampleInputSlug" class="form-label">Slug</label>
                    <input name="slug" type="text" class="form-control" id="exampleInputSlug"  value="{{ old('slug') }}" required>
                    <div id="nameHelp" class="form-text"></div>
                    @error('slug')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Сохранить категорию</button>
            </form>
        </div>
    </main>
@endsection

