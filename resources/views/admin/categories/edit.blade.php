@extends('admin.layouts.layout')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Редактирование {{ $category->title }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('admin.categories.index') }}" class="btn btn-primary">Назад</a>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <form method="POST" action="{{ route('admin.categories.update', ['category' => $category->id]) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="exampleInputTitle" class="form-label">Название</label>
                <input name="title" type="text" class="form-control" id="exampleInputTitle" value="{{ $category->title }}" required>
                <div id="nameHelp" class="form-text"></div>
                @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="exampleInputSlug" class="form-label">Slug</label>
                <input name="slug" type="text" class="form-control" id="exampleInputSlug"  value="{{ $category->slug }}" required>
                <div id="nameHelp" class="form-text"></div>
                @error('slug')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Сохранить категорию</button>
        </form>
    </div>
    </main>
@endsection

