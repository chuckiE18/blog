@extends('admin.layouts.layout')

@section('content')
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Создание поcта</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-toolbar mb-2 mb-md-0">
                    <a href="{{ route('admin.posts.index') }}" class="btn btn-primary">Назад</a>
                </div>
            </div>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-lg-4">
            <form method="POST" action="{{ route('admin.posts.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputTitle" class="form-label">Название</label>
                    <input name="title" type="text" class="form-control" id="exampleInputTitle" value="{{ old('title') }}" required>
                    <div id="nameHelp" class="form-text"></div>
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="exampleInputSlug" class="form-label">Slug</label>
                    <input name="slug" type="text" class="form-control" id="exampleInputSlug"  value="{{ old('slug') }}" required>
                    <div id="nameHelp" class="form-text"></div>
                    @error('slug')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="exampleFormControlTextarea1">Описание</label>
                    <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="2" required>{{ old('description') }}</textarea>
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="content">Контент</label>
                    <textarea name="content" class="form-control" id="content" rows="3">{{ old('content') }}</textarea>
                    @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="picture">Изображение</label>
                    <input name="picture" type="file" class="form-control-file" id="picture" required>
                    @error('picture')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <select class="form-control" name="category_id" required>
                        <option>Выберите категорию</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                    @endforeach
                    </select>
                    @error('category_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Сохранить пост</button>
            </form>
        </div>
    </main>
@endsection

@section('scripts')
    <script>
        ClassicEditor
            .create( document.querySelector( '#content' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection
