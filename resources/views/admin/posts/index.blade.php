@extends('admin.layouts.layout')

@section('content')
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Список постов</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('admin.posts.create') }}" class="btn btn-primary">Добавить пост</a>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Slug</th>
                    <th scope="col">Описание</th>
                    <th scope="col">Контент</th>
                    <th scope="col">Категория</th>
                    <th scope="col">Дата создания</th>
                    <th scope="col">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->slug }}</td>
                        <td>{{ $post->description }}</td>
                        <td>{{ $post->content }}</td>
                        <td>{{ $post->category->title }}</td>
                        <td>{{ $post->created_at }}</td>
                        <td>
                            <a href="{{ route('admin.posts.edit', ['post' => $post->id]) }}" class="btn btn-warning">Редактировать</a>
                            <form action="{{ route('admin.posts.destroy', ['post' => $post->id]) }}" method="POST" style="display: inline">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger" value="Удалить">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
                {{ $posts->links('vendor.pagination.bootstrap-4') }}
        </div>
    </main>
@endsection
