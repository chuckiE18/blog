@extends('admin.layouts.layout')

@section('content')
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Создание пользователя</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-toolbar mb-2 mb-md-0">
                    <a href="{{ route('users.index') }}" class="btn btn-primary">Назад</a>
                </div>
            </div>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-lg-4">
            <form method="POST" action="{{ route('users.store') }}">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputName" class="form-label">Имя</label>
                    <input name="name" type="text" class="form-control" id="exampleInputName" aria-describedby="emailHelp" value="{{ old('name') }}">
                    <div id="nameHelp" class="form-text"></div>
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email</label>
                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ old('email') }}">
                    <div id="emailHelp" class="form-text"></div>
                    @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Пароль</label>
                    <input name="password" type="text" class="form-control" id="exampleInputPassword1">
                </div>
                @error('password')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="mb-3 form-check">
                    <input name="is_admin" type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Администратор</label>
                </div>
                <button type="submit" class="btn btn-primary">Создать</button>
            </form>
        </div>
    </main>
@endsection
