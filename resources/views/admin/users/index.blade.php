@extends('admin.layouts.layout')

@section('content')

        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Пользователи</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('users.create') }}" class="btn btn-primary">Добавить пользователя</a>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Email</th>
                    <th scope="col">Админ</th>
                    <th scope="col">Дата регистрации</th>
                    <th scope="col">Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->is_admin }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>
                            <a href="{{ route('users.edit', ['user' => $user->id]) }}" class="btn btn-warning">Редактировать</a>
                            <form action="{{ route('users.destroy', ['user' => $user->id]) }}" method="POST" style="display: inline">
                                @csrf
                                @method('DELETE')
{{--                                <input type="hidden" name="user_id" value="{{ $user->id }}">--}}
                                <input type="submit" class="btn btn-danger" value="Удалить">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
                {{ $users->links('vendor.pagination.bootstrap-4') }}
        </div>
    </main>
@endsection
