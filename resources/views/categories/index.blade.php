@extends('layouts.layout')

@section('title')
    <h1>Categories</h1>
@endsection

@section('background')
    {{ asset('assets/img/home-bg.jpg') }}
@endsection

@section('content')
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <!-- Post preview-->
                @foreach($categories as $category)
                    <div class="post-preview">
                        <a href="{{ route('categories.show', ['slug' => $category->slug]) }}">
                            <h2 class="post-title">{{ $category->title}}</h2>
                        </a>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                @endforeach
                <!-- Pager-->
                <div class="d-flex justify-content-center mb-4">
                    {{ $categories->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
@endsection
