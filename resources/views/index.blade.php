@extends('layouts.layout')

@section('title')
    <h1>Clean Blog</h1>
    <span class="subheading">A Blog Theme by Start Bootstrap</span>
@endsection

@section('background')
    {{ asset('assets/img/home-bg.jpg') }}
@endsection

@section('content')
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <!-- Post preview-->
                @foreach($posts as $post)
                    <div class="post-preview">
                        <a href="{{ route('post.show', ['slug' => $post->slug]) }}">
                            <h2 class="post-title">{{ $post->title }}</h2>
                            <h3 class="post-subtitle">{{ $post->description }}</h3>
                        </a>
                        <p class="post-meta">
                            Category:
                            <a href="{{ route('categories.show', ['slug' => $post->category->slug]) }}">{{ $post->category->title }}</a>; {{ $post->created_at }}
                        </p>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                @endforeach
                <!-- Pager-->
                <div class="d-flex justify-content-center mb-4">
                    {{ $posts->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
@endsection
