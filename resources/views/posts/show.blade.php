@extends('layouts.layout')

@section('title')
    <h1>{{ $post->title }}</h1>
    <h2 class="subheading">{{ $post->description }}</h2>
    <span class="meta"> Category
        <a href="#!">{{ $post->category->title }}</a> on {{ $post->created_at }}
    </span>
@endsection

@section('background')
    {{ asset($post->picture) }}
@endsection

@section('content')
    <article class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    {!! $post->content !!}
                </div>
            </div>
        </div>
    </article>
@endsection
