<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminPostController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\AdminCategoryController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin.index');
    Route::resource('/users', AdminUserController::class);
    Route::resource('/posts', AdminPostController::class, [
        'as' => 'admin'
    ]);
    Route::resource('/categories', AdminCategoryController::class, [
        'as' => 'admin'
    ]);
});

Route::get('/login', [UserController::class, 'loginForm'])->name('loginForm');
Route::post('/login', [UserController::class, 'login'])->name('login');
Route::get('/register', [UserController::class, 'create'])->name('create');
Route::post('/register', [UserController::class, 'store'])->name('store');
Route::get('/logout', [UserController::class, 'logout'])->name('logout');

Route::get('/',[PostController::class, 'index'])->name('home');
Route::get('/post/{slug}',[PostController::class, 'show'])->name('post.show');
Route::get('/categories/{slug}', [CategoryController::class, 'show'])->name('categories.show');
Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
Route::get('/about', function() {
   return view('about');
})->name('about');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('/contact/store', [ContactController::class, 'store'])->name('contact.request.store');
